package com.mycompany.test2.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.mycompany.test2.models.Deuda;

@Repository
public interface DeudaRepositorie extends CrudRepository<Deuda, String> {

}
