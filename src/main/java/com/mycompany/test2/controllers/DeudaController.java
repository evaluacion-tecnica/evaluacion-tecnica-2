package com.mycompany.test2.controllers;

import java.awt.List;
import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.test2.models.Deuda;
import com.mycompany.test2.services.DeudaService;

@RestController
@RequestMapping("/")
class DeudaController {
	@Autowired
	DeudaService deudaService;
	
	@GetMapping
	public ArrayList<Deuda> obtenerDeudas(){
		return deudaService.obtenerDudas();
	}
	
	@GetMapping(path = "/{id}")
	public Optional<Deuda> buscarDeudaPorId(@PathVariable("id") String id){
		return this.deudaService.buscarDeudaPorIdClient(id);
	}
	
	@PostMapping
	public Deuda guardarDeuda(@RequestBody Deuda deuda) {
		return this.deudaService.guardarDeuda(deuda);
	}
	
	@DeleteMapping(path = "/{id}")
	public String eliminarDeuda(@PathVariable("id") String idClient) {
		boolean ok = this.deudaService.eliminarDeuda(idClient);
		if (ok) {
			return "Deuda eliminada" + idClient; 
		} else {
			return "No se pudo eliminar la deuda";
		}
	}

}
