package com.mycompany.test2.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "deuda")
public class Deuda {
	
	@Id
	@Column(name = "idclient")
	private String idCliente;
	@Column(name = "nameclient")
	private String nombreCliente;
	@Column(name = "mail")
	private String Correo;
	@Column(name = "amountdebt")
	private int montoDeuda;
	@Column(name = "iddebt")
	private String idDeuda;
	@Column(name = "debtdate")
	private String fecha;

	public Deuda(String idCliente, String nombreCliente, String Correo, int montoDeuda, String idDeuda, String fecha) {
	     this.idCliente = idCliente;
	     this.nombreCliente = nombreCliente;
	     this.Correo = Correo;
	     this.montoDeuda = montoDeuda;
	     this.idDeuda = idDeuda;
	     this.fecha = fecha;
	}

	public Deuda() {
	}
	    

	public String getIdCliente() {
		return idCliente;
	}

	    public void setIdCliente(String idCliente) {
	        this.idCliente = idCliente;
	    }

	    public String getNombreCliente() {
	        return nombreCliente;
	    }

	    public void setNombreCliente(String nombreCliente) {
	        this.nombreCliente = nombreCliente;
	    }

	    public String getCorreo() {
	        return Correo;
	    }

	    public void setCorreo(String Correo) {
	        this.Correo = Correo;
	    }

	    public int getMontoDeuda() {
	        return montoDeuda;
	    }

	    public void setMontoDeuda(int montoDeuda) {
	        this.montoDeuda = montoDeuda;
	    }

	    public String getIdDeuda() {
	        return idDeuda;
	    }

	    public void setIdDeuda(String idDeuda) {
	        this.idDeuda = idDeuda;
	    }

	    public String getFecha() {
	        return fecha;
	    }

	    public void setFecha(String fecha) {
	        this.fecha = fecha;
	    }

	    @Override
	    public String toString() {
	        return "deudas{" + "idCliente=" + idCliente + ", nombreCliente=" + nombreCliente + ", Correo=" + Correo + ", montoDeuda=" + montoDeuda + ", idDeuda=" + idDeuda + ", fecha=" + fecha + '}';
	    }
	    
	    

}
