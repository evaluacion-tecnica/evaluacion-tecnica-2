package com.mycompany.test2.services;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mycompany.test2.models.Deuda;
import com.mycompany.test2.repositories.DeudaRepositorie;

@Service
public class DeudaService {
	@Autowired
	DeudaRepositorie deudaRepositorie;
	
	public ArrayList<Deuda> obtenerDudas(){
		return (ArrayList<Deuda>) deudaRepositorie.findAll();
	}
	
	public Optional<Deuda> buscarDeudaPorIdClient(String id){
		return deudaRepositorie.findById(id);
	}
	
	public Deuda guardarDeuda(Deuda deuda) {
		return deudaRepositorie.save(deuda);
	}
	
	public boolean eliminarDeuda(String idClient) {
		try {
			deudaRepositorie.deleteById(idClient);
			return true;
		} catch (Exception err) {
			return false;
		}
	}

}
